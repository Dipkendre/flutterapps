import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: QuizApp(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class QuizApp extends StatefulWidget {
  const QuizApp({super.key});

  @override
  State<QuizApp> createState() => _QuizAppState();
}

class Questionmodel {
  final String? question;
  final List<String>? options;
  final int? answerindex;
  const Questionmodel({this.question, this.options, this.answerindex});
}

class _QuizAppState extends State<QuizApp> {
  int questionindex = 0;
  int selectedanswer = -1;
  int noOfCorrectAnswer = 0;
  int screen = 0;

  List questions = [
    const Questionmodel(
      question: "Who developed the Flutter Framework  ?",
      options: ["Facebook", "Google", "Microsoft", "Oracal"],
      answerindex: 1,
    ),
    const Questionmodel(
      question:
          "Which programming language is used to build Flutter applications?",
      options: ["Kotlin", "Java", "Go", "Dart"],
      answerindex: 3,
    ),
    const Questionmodel(
      question: "A sequence of asynchronous Flutter events is known as a: ?",
      options: ["Flow", "Stream", "Current", "Series"],
      answerindex: 1,
    ),
    const Questionmodel(
      question:
          "What language is Flutter's rendering engine primarily written in?",
      options: ["c++", "kotlin", "Dart", "Java"],
      answerindex: 0,
    ),
    const Questionmodel(
      question: "What widget would you use for repeating content in Flutter?",
      options: ["ListView", "ExpandedView", "Stack", "ArrayView"],
      answerindex: 0,
    ),
  ];

  MaterialStateProperty<Color?> checkAnswer(int buttonindex) {
    if (selectedanswer != -1) {
      if (buttonindex == questions[questionindex].answerindex) {
        return const MaterialStatePropertyAll(Colors.green);
      } else if (buttonindex == selectedanswer) {
        return const MaterialStatePropertyAll(Colors.red);
      } else {
        return const MaterialStatePropertyAll(null);
      }
    } else {
      return const MaterialStatePropertyAll(null);
    }
  }

  void validatePage() {
    if (selectedanswer == -1) {
      return;
    }
    if (selectedanswer == questions[questionindex].answerindex) {
      noOfCorrectAnswer += 1;
    }
    if (selectedanswer != -1) {
      if (questionindex == questions.length - 1) {
        setState(() {
          screen = 2;
        });
      }
      selectedanswer = -1;
      setState(() {
        questionindex += 1;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    if (screen == 0) {
      return Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset("assets/images/quize.jpg"),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                  onPressed: () {
                    setState(() {
                      screen = 1;
                    });
                  },
                  child: const Text("Start Quiz"))
            ],
          ),
        ),
      );
    }
    if (screen == 1) {
      return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blue,
          title: const Text(
            "Quiz App",
            style: TextStyle(
              fontSize: 30,
              fontWeight: FontWeight.w600,
              color: Colors.orange,
            ),
          ),
          centerTitle: true,
        ),
        body: Column(
          children: [
            const SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text(
                  "Questions : ",
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                Text(
                  "${questionindex + 1}/${questions.length}",
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w600,
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 30,
            ),
            SizedBox(
              width: 380,
              height: 70,
              child: Text(
                questions[questionindex].question,
                style: const TextStyle(
                  fontSize: 23,
                  fontWeight: FontWeight.w400,
                ),
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            ElevatedButton(
              onPressed: () {
                if (selectedanswer == -1) {
                  setState(() {
                    selectedanswer = 0;
                  });
                }
              },
              style: ButtonStyle(
                  minimumSize: MaterialStateProperty.all(const Size(350, 50)),
                  backgroundColor: checkAnswer(0)),
              child: Text(
                "A.${questions[questionindex].options[0]}",
                style: const TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.normal,
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
              onPressed: () {
                if (selectedanswer == -1) {
                  setState(() {
                    selectedanswer = 1;
                  });
                }
              },
              style: ButtonStyle(
                  minimumSize: MaterialStateProperty.all(const Size(350, 50)),
                  backgroundColor: checkAnswer(1)),
              child: Text(
                "B.${questions[questionindex].options[1]}",
                style: const TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.normal,
                ),
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            ElevatedButton(
              onPressed: () {
                if (selectedanswer == -1) {
                  setState(() {
                    selectedanswer = 2;
                  });
                }
              },
              style: ButtonStyle(
                  minimumSize: MaterialStateProperty.all(const Size(350, 50)),
                  backgroundColor: checkAnswer(2)),
              child: Text(
                "C.${questions[questionindex].options[2]}",
                style: const TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.normal,
                ),
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            ElevatedButton(
              onPressed: () {
                if (selectedanswer == -1) {
                  setState(() {
                    selectedanswer = 3;
                  });
                }
              },
              style: ButtonStyle(
                  minimumSize: MaterialStateProperty.all(const Size(350, 50)),
                  backgroundColor: checkAnswer(3)),
              child: Text(
                "D.${questions[questionindex].options[3]}",
                style: const TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.normal,
                ),
              ),
            ),
          ],
        ),
        floatingActionButton: FloatingActionButton(
            onPressed: () {
              validatePage();
            },
            backgroundColor: Colors.blue,
            child: const Icon(
              Icons.forward,
              color: Colors.orange,
            )),
      );
    } else {
      return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blue,
          title: const Text(
            "Quiz App",
            style: TextStyle(
              fontSize: 30,
              fontWeight: FontWeight.w600,
              color: Colors.orange,
            ),
          ),
          centerTitle: true,
        ),
        body: Center(
          child: Column(
            children: [
              const SizedBox(
                height: 20,
              ),
              Image.asset("assets/images/trophy.jpg"),
              const SizedBox(
                height: 20,
              ),
              const Text(
                "You have completed the quiz!",
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w600,
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Text(
                "Score:$noOfCorrectAnswer/${questions.length}",
                style: const TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w600,
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                onPressed: () {
                  questionindex = 0;
                  selectedanswer = -1;
                  noOfCorrectAnswer = 0;
                  screen = 0;
                  setState(() {});
                },
                child: const Text("Restart"),
              ),
            ],
          ),
        ),
      );
    }
  }
}
