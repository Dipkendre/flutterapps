// Create a Screen in which we have 3 Containers in a Column each container
// must be of height 100 and width 100. Each container must have an image
// as a child.

import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Center(
          child: Column(
            children: [
              Container(
                height: 100,
                width: 100,
                child: Image.asset("lib/assets/images/dk.jpg"),
              ),
              Container(
                height: 100,
                width: 100,
                child: Image.asset("lib/assets/images/dk.jpg"),
              ),
              Container(
                height: 100,
                width: 100,
                child: Image.asset("lib/assets/images/dk.jpg"),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
