// Create a Screen and add your image in the center of the screen below your
// image display your name in a container, give a shadow to the Container
// and give a border to the container the top left and top right corners must
// be circular, with a radius of 20. Add appropriate padding to the container.

import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Center(
          child: Container(
            padding: const EdgeInsets.all(20.0),
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20),
                topRight: Radius.circular(20),
              ),
            ),
            child: Column(
              children: [
                Image.asset("lib/assets/images/Passphoto.png"),
                const SizedBox(
                  height: 30,
                  width: 30,
                ),
                const Text(
                  "Dipak Kendre",
                  style: TextStyle(
                    fontSize: 30,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
