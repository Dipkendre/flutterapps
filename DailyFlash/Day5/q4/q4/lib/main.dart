// Create a Screen in which we will display 3 Containers of Size 100,100 in a
// Row. Give color to the containers. The containers must divide the free
// space in the main axis evenly among each other.

import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: const Text("Daily Flash"),
        ),
        body: Center(
          child: Column(
            children: [
              const SizedBox(
                height: 100,
                width: 100,
              ),
              Container(
                width: 100,
                height: 100,
                color: Colors.red,
              ),
              const SizedBox(
                height: 100,
                width: 100,
              ),
              Container(
                width: 100,
                height: 100,
                color: Colors.amber,
              ),
              const SizedBox(
                height: 100,
                width: 100,
              ),
              Container(
                width: 100,
                height: 100,
                color: Colors.green,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
