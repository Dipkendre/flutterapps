import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Center(
          child: ContainerColor(),
        ),
      ),
    );
  }
}

class ContainerColor extends StatefulWidget {
  @override
  _ContainerColor createState() => _ContainerColor();
}

class _ContainerColor extends State<ContainerColor> {
  String text = 'Click me!';
  Color containerColor = Colors.red;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          if (text == 'Click me!') {
            text = 'Container Tapped';
            containerColor = Colors.blue;
          } else {
            text = 'Click me!';
            containerColor = Colors.red;
          }
        });
      },
      child: Container(
        width: 200,
        height: 200,
        color: containerColor,
        child: Center(
          child: Text(
            text,
            style: const TextStyle(
              fontSize: 20.0,
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }
}
