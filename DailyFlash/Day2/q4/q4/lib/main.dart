import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Center(
          child: Container(
            height: 100,
            width: 150,
            decoration: BoxDecoration(
              color: const Color.fromARGB(255, 226, 138, 139),
              border: Border.all(color: Colors.black),
              borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(10),
                  bottomRight: Radius.circular(10)),
            ),
            child: const SizedBox(
              height: 10,
              width: 10,
              child: Padding(
                padding: EdgeInsets.all(15),
                child: Text("Dipak P Kendre"),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
