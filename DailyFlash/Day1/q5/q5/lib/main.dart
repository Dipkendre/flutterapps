import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Center(
          child: Container(
            width: 200,
            height: 200,
            decoration: const BoxDecoration(
              border: Border.symmetric(),
              color: Colors.orange,
              boxShadow: [
                BoxShadow(
                  color: Colors.red,
                  blurRadius: 10,
                  //      offset: Offset(1.1),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
