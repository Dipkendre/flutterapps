import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.amber,
          leading: IconButton(
            icon: const Icon(Icons.send),
            onPressed: () {},
          ),
          actions: <Widget>[
            IconButton(
              icon: const Icon(Icons.save),
              // icon: const Icon(Icons.delete),
              onPressed: () {},
            ),
            IconButton(
              // icon: const Icon(Icons.save),
              icon: const Icon(Icons.delete),
              onPressed: () {},
            ),
            IconButton(
              // icon: const Icon(Icons.save),
              icon: const Icon(Icons.circle),
              onPressed: () {},
            ),
          ],
        ),
      ),
    );
  }
}
