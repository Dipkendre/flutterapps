// Create a screen and add a floatingAction button. Place the floating action
// button in the bottom center of the screen. When the button is long pressed
// the color of the button must change to purple.

import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MyScreen(),
    );
  }
}

class MyScreen extends StatefulWidget {
  const MyScreen({super.key});

  @override
  _MyScreenState createState() => _MyScreenState();
}

class _MyScreenState extends State<MyScreen> {
  Color fabColor = Colors.blue;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('FloatingActionButton Example'),
      ),
      body: const Center(
        child: Text(
          'Press & Hold Button!!!!!!!!!!!',
          style: TextStyle(fontSize: 20.0),
        ),
      ),
      floatingActionButton: GestureDetector(
        onLongPress: () {
          setState(() {
            fabColor = Colors.purple;
          });
        },
        onLongPressEnd: (details) {
          setState(() {
            fabColor = Colors.blue;
          });
        },
        child: FloatingActionButton(
          onPressed: () {},
          backgroundColor: fabColor,
          child: const Icon(Icons.add),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}
