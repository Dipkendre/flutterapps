// Create an ElevatedButton, in the centre of the screen. The button must
// have rounded edges. Give a shadow of color red to the button.

import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Center(
          child: ElevatedButton(
            
            onPressed: () {},
            style: ElevatedButton.styleFrom(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
                
              ),
              shadowColor: Colors.red,
              
            ),
            child: const Text("Button"),
          ),
        ),
      ),
    );
  }
}
