// 3. Create a Screen and then add a floating action button. In this button, you
// will have to display your name and an Icon which must be placed in a row.

import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: const Center(
          child: Text(""),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {},
          child: const Row(
            children: [
              Icon(Icons.person),
              SizedBox(width: 5.0),
              Text("Dip"),
            ],
          ),
        ),
      ),
    );
  }
}
